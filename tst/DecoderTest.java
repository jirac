import org.diracvideo.Jirac.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

class CloseListener extends WindowAdapter {
    public void windowClosing(WindowEvent e) {
 	System.exit(0);
    }
}

class PictureDrawer extends Canvas implements Runnable {
    private Decoder dec;
    private int wait;
    
    public PictureDrawer(Decoder d) {
	dec = d;
	wait = (1000 * dec.format.frame_rate_denominator) / 
	    dec.format.frame_rate_numerator;
    }


    public void run() {
	Graphics gr = getGraphics();
	while(dec.status != Decoder.Status.DONE) {
	    paint(gr);
	    try {
		Thread.sleep(wait);
	    } catch(InterruptedException e) {
		e.printStackTrace();
	    }
	}
    }
    
    public void paint(Graphics gr) {
	Picture pic = dec.out.pull();
	if(pic != null &&
	   pic.error == null) {
	    gr.drawImage(pic.img,0,0,null);
	}
    }
}


class DiracAcceptor implements FileFilter {
    public boolean accept(File f) {
	String fn = f.getName();
	if(fn.length() == fn.lastIndexOf(".drc") + 4 &&
	   f.isFile() && f.canRead()) {
	    return true;
	}
	return false;
    }
}

public final class DecoderTest {
    private static Thread decoder_thread, draw_thread;
    public static void main(String a[]) {
	Decoder dec = new Decoder();
	int ev = 0, tm;
	FileInputStream in = null;
	Frame win;
	try {
	    in = tryOpen(a);
	    byte[] packet;
	    while(dec.format == null) {
		packet = readPacket(in);
		dec.push(packet, 0, packet.length);
	    }
	    win = createWindow(dec);
	    int i = 0;
	    while(in.available() > 0 && !dec.done()) {
		packet = readPacket(in);
		dec.push(packet, 0, packet.length);
		if(i++ == 200) break;
		dec.decode();
	    }
	    dec.stop();
	    in.close();
	    draw_thread.join(); 
	    win.setVisible(false);
	    win.dispose();
	} catch(IOException e) {
	    e.printStackTrace();
	    ev = 1;
	} catch(InterruptedException e) {
	    e.printStackTrace();
	} catch(Throwable e) {
	    e.printStackTrace();
	    ev = 1;
	}  finally { 
	    System.exit(ev);
	}
    }

    private static byte[] readPacket(FileInputStream in) throws IOException {
	if(true) {
	    int read = Math.min(in.available(), 1000);
	    byte packet[] = new byte[read];
	    in.read(packet);
	    return packet;
	} else {
	    byte[] header = new byte[13];
	    in.read(header);
	    Unpack u = new Unpack(header);
	    if(u.decodeLit32() != 0x42424344) {
		throw new IOException("Cannot parse dirac stream");
	    } 
	    if(u.bits(8) == 0x10) {
		return header;
	    }
	    int size = u.decodeLit32();
	    byte[] packet = new byte[size];
	    System.arraycopy(header, 0, packet, 0, 13);
	    in.read(packet, 13, size - 13);
	    return packet;
	}
    }


    private static FileInputStream tryOpen(String a[]) throws IOException {
	for(int i = 0; i < a.length; i++) {
	    File f = new File(a[i]);
	    if (f.canRead()) {
		return new FileInputStream(f);
	    }
	}
	File[] files = new File(".").listFiles(new DiracAcceptor());
	for(int i = 0; i < files.length; i++) {
	    try {
		return new FileInputStream(files[i]);
	    } catch(IOException e) {
		e.printStackTrace();
	    }
	}
	System.err.println("No dirac file was found");
	System.exit(0);
	return null;
    }

    private static Frame createWindow(Decoder dec) {
	Frame fr = new Frame("DecoderTest");
	PictureDrawer cn = new PictureDrawer(dec);
	WindowListener wl = new CloseListener();
	cn.setSize(dec.format.width, dec.format.height);
	fr.add(cn);
	fr.pack();
	fr.addWindowListener(wl);
	fr.setVisible(true);
	draw_thread = new Thread(cn);
	draw_thread.start();
	return fr;
    }


}
