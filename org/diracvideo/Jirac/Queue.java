package org.diracvideo.Jirac;

/** Synchronized Picture Queue. */

class Node {
    Picture load;
    Node next;
}

public class Queue {
    protected Node free, head, tail;
    protected synchronized Node getNode() {
	Node nd;
	if(free == null) 
	    nd = new Node();
	else {
	    nd = free;
	    free = free.next;
	    nd.next = null;
	}
	return nd;
    }

    protected synchronized void removeNode(Node nd) {
	if(nd == head) 
	    head = head.next;
	else 
	    for(Node pr = head; pr != null; pr = pr.next)
		if(pr.next == nd) {
		    pr.next = nd.next;
		    if(nd.next == null) tail = pr;
		    break;
		}
	nd.next = free;
	free = nd;
	nd.load = null;
    }
    
    public synchronized void push(Picture p) {
	Node nd = getNode();
	nd.load = p;
	if(head == null) 
	    head = tail = nd;
	else {
	    tail.next = nd;
	    tail = nd;
	}
    }
    
    public synchronized Picture pull() {
	if(empty()) return null;
	Node nd = head;
	Picture p = nd.load;
	removeNode(head);
	return p;
    }

    public boolean empty() {
	return head == null;
    }
    
    public synchronized boolean cyclic() {
	if(head == null)
	    return false;
	Node rabbit = head.next, turtle = head;
	while(rabbit != null &&
	      turtle != null) {
	    if(rabbit == turtle ||
	       turtle == rabbit.next) 
		return true;
	    turtle = turtle.next;
	    rabbit = rabbit.next;
	    if(rabbit != null)
		rabbit = rabbit.next;
	}
	return false; 
    }

    public synchronized void flush() {
	if(head == null) return;
	for(Node nd = head; nd != null; nd = nd.next) {
	    nd.load = null;
	    if(nd.next == null) {
		nd.next = free;
		break;
	    }
	}
	free = head;
	head = tail = null;
    }

    public synchronized String toString() {
	if(cyclic()) { return "CYCLIC"; }
	StringBuilder sb = new StringBuilder();
	for(Node nd = head; nd != null; nd = nd.next) {
	    sb.append(String.format("%s --> ", nd));
	}
	sb.append("NULL");
	return sb.toString();
    }

}

class InputQueue extends Queue {
    public synchronized boolean empty() {
	for(Node nd = head; nd != null; nd = nd.next)
	    if(nd.load.decodable()) return false;
	return true; 
    }

    public synchronized Picture pull() {
	Picture p = null;
	for(Node nd = head; nd != null; nd = nd.next) 
	    if(nd.load.decodable()) {
		p = nd.load;
		removeNode(nd);
		break;
	    } 
	return p;
    }

}

class OutputQueue extends Queue {
    public synchronized void push(Picture pic) {
	Node nd = getNode();
	assert pic != null;
	nd.load = pic;
	if(head == null) {
	    head = nd;
	} else if(head.load.num > pic.num) {
	    nd.next = head; head = nd;
	}  else { 
	    for(Node pr = head, cmp = head.next; 
		cmp != null; cmp = cmp.next)  
		if(cmp.load.num > pic.num) {
		    pr.next = nd;
		    nd.next = cmp;
		    return;
		} else if(cmp.next == null) {
		    cmp.next = nd;
		} else pr = cmp;
	} 
	
    }
}