package org.diracvideo.Jirac;

/** 
 * Decoder
 *
 * An interface to decoding a dirac stream. 
 * Most (all) of the actual work is done by the
 * Picture class, however Decoder can do general
 * dispatching, scheduling and bookkeeping.
 * That is the reason we keep it arround */

public class Decoder implements Runnable {
    private Stream stream;
    public VideoFormat format;
    public Status status = Status.NULL;
    public Exception e;
    public Queue in, out;
    public Cache refs;
    public enum Status {NULL, OK, WAIT, DONE, ERROR}

    public Decoder() {
	stream = new Stream();
	refs = new Cache(4);
	in = new InputQueue();
	out = new OutputQueue();
    }
    
    /** Push:
     * @param d byte array containing stream data
     * @param o offset in the byte array
     * @param l length of segment */

    public void push(byte d[], int o, int l) throws Exception {
	push(new Buffer(d,o,l));
    }


    private void push(Buffer buf) throws Exception {
	// HOI BIE...NIET TEVEEL MOPPEREN VANDAAG HE! :)
	stream.add(buf);
	for(Buffer packet = stream.next(); 
	    packet != null;
	    packet = stream.next())
	    dispatch(packet);
	if(!in.empty() && status  == Status.WAIT)
	    synchronized(this) {  notify(); }
    }


    /* at this point, the buffer must be a complete dirac packet */
    private void dispatch(Buffer b) throws Exception {
	assert (b.getInt(5) == b.size()) : "Incorrect buffer sizes";
	byte c = b.getByte(4);
	switch(c) {
	case 0x00:
	    VideoFormat tmp = new VideoFormat(b);
	    if(format == null) {
		format = tmp;
		status = Status.OK;
	    } else if(!tmp.equals(format)) {
		throw new Exception("Stream Error: Inequal Video Formats");
	    }
	    break;
	case 0x10:
	    status = Status.DONE;
	    break;
	case 0x20:
	case 0x30:
	    break;
	default:
	    if(format == null) 
		throw new Exception("Stream Error: Picture Before Header");
	    Picture pic = new Picture(b, this);
	    pic.parse();
	    in.push(pic);
	    break;
	}
    }

    public synchronized void decode() {
	while(!in.empty()) {
	    Picture pic = in.pull();
	    pic.decode();
	    if(pic.error == null) {
		out.push(pic); 
	    } else pic.error.printStackTrace();
	}
    }

    /** A decoding loop */
    public synchronized void run() {
	while(!done()) {
	    decode();
	    status = Status.WAIT;
	    try { wait(); }  catch(InterruptedException ex) {}
	    status = Status.OK;
	}
    }

    public boolean done() {
	return status == Status.DONE && in.empty();
    }

    public void stop() {
	status = Status.DONE;
	in.flush();
	out.flush();
	stream.flush();
	refs.flush();
    }
}

