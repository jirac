package org.diracvideo.Jirac;

class Cache {
    private int end, nums[];
    private Block blocks[][], scaled[][];

    public Cache(int n) {
	end = 0;
	nums = new int[n];
	blocks = new Block[n][];
	scaled = new Block[n][];
    }

    public synchronized void add(int n, Block ref[]) {
	if(end == nums.length) 
	    shiftFrom(0);
	nums[end] = n;
	blocks[end] = ref;
	end++;
    }
    
    public synchronized Block[] get(int n, boolean upscaled) {
	int i = getIndex(n);
	if(i < 0) return null;
	if(upscaled) {
	    if(scaled[i] == null) {
		scaled[i] = new Block[blocks[i].length];
		for(int j = 0; j < scaled[i].length; j++)
		    scaled[i][j] = blocks[i][j].upSample();
	    }
	    return scaled[i];
	} 
	return blocks[i];
    } 

    public synchronized void remove(int n) {
	int i = getIndex(n);
	if(i >= 0) shiftFrom(i);
    }
    
    public boolean has(int n) {
	return getIndex(n) >= 0;
    }
    
    private synchronized void shiftFrom(int i) {
	while(++i < end) {
	    nums[i-1] = nums[i];
	    blocks[i-1] = blocks[i];
	    scaled[i-1] = scaled[i];
	}
	end--;
	nums[end] = -1;
	blocks[end] = scaled[end] = null;
    }

    private synchronized int getIndex(int n) {
	for(int i = 0; i < end; i++)
	    if(nums[i] == n) return i;
	return -1;
    }

    public synchronized void flush() {
	while(end > 0) shiftFrom(end);
    }
}