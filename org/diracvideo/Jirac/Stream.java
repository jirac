package org.diracvideo.Jirac;

class Stream {
    private int prev;
    private Buffer next;

    public synchronized void add(Buffer buf) {
	next = (next == null ? buf : next.cat(buf));
    }

    public synchronized Buffer next() {
	if(next == null) return null;
	int size = next.size();
	if(size < 13) return null;
	if(next.getInt(0) != 0x42424344) {
	    System.err.println("Not reading a dirac stream");
	    flush();
	    return null;
	}
	int offset = next.getInt(5);
	if(offset == 0) 
	    offset = 13;
	if(offset > size) return null;
	assert prev == next.getInt(9);
	prev = offset;
	if(size == offset) {	
	    Buffer tmp = next;
	    next = null;
	    return tmp;
	} else {
	    Buffer tmp = next.sub(0, offset);
	    next = next.sub(offset);
	    return tmp;
	}
    }

    public synchronized void flush() {
	next = null;
	prev = 0;
    }
}
