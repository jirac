package org.diracvideo.Jirac;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

public class ColourSpace {
    private VideoFormat format;
    private int rgb_table[][], matrix[][], rgb_pixels[];
    private int decodeMode;

    public ColourSpace(int colourmode, VideoFormat fmt) {
	format = fmt;
	rgb_pixels = new int[fmt.width*fmt.height];
	decodeMode = fmt.chromaHShift() | (fmt.chromaVShift() << 1);
	setupTables();
    }

    private void setupTables() {
	matrix = new int[3][3];
	rgb_table = new int[9][255];
	setupMatrix(0.2990, 0.1140);
	for(int c = 0; c < 3; c++) {
	    for(int i = 0; i < 255; i++) {
		rgb_table[3*c][i] = (matrix[0][c]*i)>>16;
		rgb_table[3*c+1][i] = (matrix[1][c]*i)>>16;
		rgb_table[3*c+2][i] = (matrix[2][c]*i)>>16;
	    }
	}
    }
    
    private void setupMatrix(double kr, double kb) {
	double kg = 1.0 - kr - kb;
	int factor = 1 << 16;
	matrix[0][0] = matrix[1][0] = matrix[2][0] = factor;
	matrix[0][2] = (int)(2*(1-kr)*factor);
	matrix[1][1] = (int)((-2*kb*(1-kb)/kg)*factor);
	matrix[1][2] = (int)((-2*kr*(1-kr)/kg)*factor);
	matrix[2][1] = (int)(2*(1-kb)*factor);
    }

    public synchronized void convert(Block yuv[], BufferedImage img) {
	Dimension lum = yuv[0].s;
	short[] Y = yuv[0].d, U = yuv[1].d, V = yuv[2].d;
	switch(decodeMode) {
	case 0: /* yShift = 0, xShift = 0 */
	    for(int y = 0; y < format.height; y++)  {
		int rgbLine = y*format.width;
		int yLine = yuv[0].line(y);
		for(int x = 0; x < format.width; x++) 
		    rgb_pixels[rgbLine+x] = YUVtoRGB(Y[yLine+x],U[yLine+x],
						     V[yLine+x]);
	    }

	    break;
	case 1: /* yShift = 0, xShift = 1 */
	    for(int y = 0; y < format.height; y++) {
		int rgbLine = y*format.width;
		int yLine = yuv[0].line(y);
		for(int x = 0; x < format.width; x++) 
		    rgb_pixels[rgbLine+x] = YUVtoRGB(Y[yLine+x],U[yLine+x/2],
						     V[yLine+x/2]);
	    }
	    break;
	case 2: /* yShift = 1, xShift = 0 */
	    for(int y = 0; y < format.height; y++) {
		int rgbLine = y*format.width;
		int yLine = yuv[0].line(y);
		int uvLine = yuv[1].line(y/2);
		for(int x = 0; x < format.width; x++) 
		    rgb_pixels[rgbLine+x] = YUVtoRGB(Y[yLine+x],U[uvLine+x],
						     V[uvLine+x]);
	    }
	    break;
	case 3: /* yShift = 1, xShift = 1 */
	    for(int y = 0; y < format.height; y++) {
		int rgbLine = y*format.width;
		int yLine = yuv[0].line(y);
		int uvLine = yuv[1].line(y/2);
		for(int x = 0; x < format.width; x++) 
		    rgb_pixels[rgbLine+x] = YUVtoRGB(Y[yLine+x],U[uvLine+x/2],
						     V[uvLine+x/2]);
	    }
	    break;
	case 4: /* yShift = 2, xShift = 0 */
	    for(int y = 0; y < format.height; y++) {
		int rgbLine = y*format.width;
		int yLine = yuv[0].line(y);
		int uvLine = yuv[1].line(y/4);
		for(int x = 0; x < format.width; x++) 
		    rgb_pixels[rgbLine+x] = YUVtoRGB(Y[yLine+x],U[uvLine+x],
						     V[uvLine+x]);
	    }
	    break;
	case 5: /* yShift = 2, xShift = 1 */
	    for(int y = 0; y < format.height; y++) {
		int rgbLine = y*format.width;
		int yLine = yuv[0].line(y);
		int uvLine = yuv[1].line(y/4);
		for(int x = 0; x < format.width; x++) 
		    rgb_pixels[rgbLine+x] = YUVtoRGB(Y[yLine+x],U[uvLine+x/2],
						     V[uvLine+x/2]);
	    }
	    break;
	}
	img.setRGB(0,0, format.width, format.height,
		   rgb_pixels, 0, format.width);
    }

    private int YUVtoRGB(short y, short u, short v) {
	return Util.clamp((y+128),0,255)*0x010101;
    }
    
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("org.diracvideo.Jirac.ColourSpace");
	for(int i = 0; i < 3; i++) {
	    sb.append(String.format("\n%d\t%d\t%d", matrix[i][0],
				    matrix[i][1], matrix[i][2]));
	}
	return sb.toString();
    }
}
















